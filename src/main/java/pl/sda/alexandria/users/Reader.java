package pl.sda.alexandria.users;

import pl.sda.alexandria.Items.ItemToBorrow;

import java.util.ArrayList;
import java.util.List;

public class Reader extends User {
    List<ItemToBorrow> borrowedItems = new ArrayList<>();

    public Reader(String name, String surname, String email, int phoneNumber) {
        super(name, surname, email, phoneNumber);
    }

    public List<ItemToBorrow> getBorrowedItems() {
        return borrowedItems;
    }

    public void borrowBook(int id) {
        borrowItem(library.getBooks(), id);
    }

    private <T extends ItemToBorrow> void borrowItem(List<T> items, int id) {
        if (items.size() == 0) {
            System.out.println("We don't have this type of items in our resources");
        }
        for (ItemToBorrow item: items) {
            if (item.getId() == id) {
                if (item.isAvailable()) {
                    item.borrow();
                    item.setReader(this);
                    borrowedItems.add(item);
                    System.out.println(this + " borrowed " + item.getType() + ": " + item.getTitle());
                } else {
                    System.out.println(item.getTitle() + " is borrowed by " + item.getReader() + ". Try later");
                }
                return;
            }
        }
        System.out.println("We don't have " + items.get(0).getType() + " with id " + id + ".");
        throw new NullPointerException("We don't have " + items.get(0).getType() + " with id " + id + ".");
    }

    public void returnItem(ItemToBorrow item) {
        System.out.printf("%s returned item: %s%n", this, item);
        item.giveBack();
        borrowedItems.remove(item);
    }

    public String showBorrowedItems() {
        List<ItemToBorrow> items = getBorrowedItems();
        if (items.size() == 0) {
            return this + " didn't borrow anything yet.";
        } else {
            String result = "Items borrowed by " + this + ": \n";
            for (ItemToBorrow item : items) {
                result += item + "\n";
            }
            return result;
        }
    }



}
