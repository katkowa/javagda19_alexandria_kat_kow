package pl.sda.alexandria.users;

import pl.sda.alexandria.AlexandriaLibrary;

public abstract class User {
    private String name;
    private String surname;
    private String email;
    private int phoneNumber;
    private String password;
    AlexandriaLibrary library = new AlexandriaLibrary();

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String name, String surname, String email, int phoneNumber) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }
}
