package pl.sda.alexandria.users;

import pl.sda.alexandria.Items.Book;
import pl.sda.alexandria.Items.BookGenre;
import pl.sda.alexandria.Items.MusicGenre;
import pl.sda.alexandria.Items.Vinyl;

public class Administrator extends User {


    public Administrator(String name, String surname, String email, int phoneNumber) {
        super(name, surname, email, phoneNumber);
    }

    public Book createBook(String title, String author, int pages, BookGenre genre) {
        return new Book(title, author, pages, genre);
    }

    public Vinyl createVinyl(String title, String author, MusicGenre genre) {
        return new Vinyl(title, author, genre);
    }
}
