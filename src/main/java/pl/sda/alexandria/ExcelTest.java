package pl.sda.alexandria;

import pl.sda.alexandria.Items.Book;
import pl.sda.alexandria.Items.BookGenre;

public class ExcelTest {
    public static void main(String[] args) {
        AlexandriaLibrary library = new AlexandriaLibrary();

        library.importData();
        Book book = new Book("title", "author", 10, BookGenre.ADVENTURE);
        library.addBook(book);
        //library.exportBooks();

        BookGenre genre1 = BookGenre.OTHER;
        System.out.println(genre1.getFromShortcut("K"));
    }
}
