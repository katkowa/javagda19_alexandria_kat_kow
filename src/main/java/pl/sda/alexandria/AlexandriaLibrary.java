package pl.sda.alexandria;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import pl.sda.alexandria.Items.*;
import pl.sda.alexandria.users.Administrator;
import pl.sda.alexandria.users.Reader;
import pl.sda.alexandria.users.User;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;


public class AlexandriaLibrary {
    private static List<User> users = new ArrayList<>();
    private static List<Book> books = new ArrayList<>();
    private static List<Vinyl> vinyls = new ArrayList<>();

    public List<Book> getBooks() {
        return new ArrayList<>(books);
    }

    public List<Vinyl> getVinyls() {
        return new ArrayList<>(vinyls);
    }

    protected List<ItemToBorrow> getBorrowableItems() {
        return new ArrayList<>(books);
    }

    public List<Book> getAvailableBooks() {
        return getAvailableItems(books);
    }

    private <T extends Item> List<T> getAvailableItems(List<T> items) {
        List<T> result = new ArrayList<>();
        for (T item : items) {
            if (item.isAvailable()) {
                result.add(item);
            }
        }
        return result;
    }

    public static List<User> getUsers() {
        return users;
    }

    public static Reader createReader(String name, String surname, String email, int phoneNumber, String password) {
        System.out.printf("Reader %s %s was created.%n", name, surname);
        Reader reader = new Reader(name, surname, email, phoneNumber);
        reader.setPassword(password);
        users.add(reader);
        return reader;
    }

    protected User getUserFromEmail(String email) {
        for (User user : users) {
            if (user.getEmail().equals(email)) {
                return user;
            }
        }
        throw new IllegalArgumentException("There is no user with email: " + email);
    }

    public static Administrator createAdministrator(String name, String surname, String email, int phoneNumber, String password) {
        System.out.printf("Administrator %s %s was created.%n", name, surname);
        Administrator administrator = new Administrator(name, surname, email, phoneNumber);
        administrator.setPassword(password);
        users.add(administrator);
        return administrator;
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void addVinyl(Vinyl vinyl) {
        vinyls.add(vinyl);
    }

    public void deleteItem(Integer id) throws Exception {
        if (getBorrowableItems().contains(getFromId(id))) {
            Item item = getFromId(id);
            if (item instanceof Book) {
                books.remove(item);
            }
            if (item instanceof Vinyl) {
                vinyls.remove(item);
            }
        } else {
            throw new Exception("We don't have item with id " + id);
        }
    }

    public void deleteBook(Book book) {
        if (book.isAvailable()) {
            books.remove(book);
        }
    }

    public void deleteVinyl(Vinyl vinyl) {
        vinyls.remove(vinyl);
    }

    public int countBooks() {
        return countItems(getBooks(), "books");
    }

    public int countVinyls() {
        return countItems(getVinyls(), "vinyls");
    }

    private <T extends Item> int countAvailableItems(List<T> items, String type) {
        int count = 0;
        for (Item item : items) {
            if (item.isAvailable()) {
                count += 1;
            }
        }
        System.out.println("Available " + type + ": " + count);
        return count;
    }

    private <T extends Item> int countItems(List<T> items, String type) {
        if (items.size() > 0) {
            System.out.print("We have " + items.size() + " " + type + " to offer. ");
            countAvailableItems(items, type);
        } else {
            System.out.println("We don't have any " + type + " to offer. ");
        }
        return items.size();
    }

    public String showBooks() {
        return showItems(books, "books");
    }

    public String showVinyls() {
        return showItems(vinyls, "vinyls");
    }

    private <T extends Item> String showItems(List<T> items, String type) {
        if (items.size() != 0) {
            String result = type + ": \n";
            for (Item item : items) {
                result += item + ", available: " + item.isAvailable() + "\n";
            }
            return result;
        } else {
            return "There is no " + type + " in our resources.";
        }
    }

    public String showAvailableBooks() {
        return showAvailableItems(books, "books");
    }

    public void showAvailableVinyls() {
        System.out.println("Available vinyls: ");
        showAvailableItems(getVinyls(), "vinyls");
    }

    private <T extends Item> String showAvailableItems(List<T> items, String type) {
        String result;
        if (items.size() != 0) {
            result = "Available " + type + ": \n";
            for (Item item : items) {
                if (item.isAvailable()) {
                    result += item + "\n";
                }
            }
        } else {
            result = "There are no " + type + " available";
        }
        return result;
    }

    public String showItemsWithExceededPeriod() {
        List<ItemToBorrow> items = getItemsWithExceededPeriod();
        if (items.size() == 0) {
            return "There isn't any item with exceeded period of borrowing.";
        } else {
            String result = "Items with exceeded period of borrowing: \n";
            for (ItemToBorrow item : items) {
                result += item + "\n";
            }
            return result;
        }
    }

    public List<ItemToBorrow> getItemsWithExceededPeriod() {
        List<ItemToBorrow> result = new ArrayList<>();
        for (ItemToBorrow item : getBorrowableItems()) {
            if (item.isPeriodExceeded()) {
                result.add(item);
            }
        }
        return result;
    }

    public < T extends Item> Item getFromId(int Id) {
        for (ItemToBorrow item : getBorrowableItems()) {
            if (item.getId() == Id) {
                return item;
            }
        }
        return null;
    }

    public void importData() throws NoSuchElementException {
        try {
            // open existing workbook
            FileInputStream inputStream = new FileInputStream(new File("src\\main\\resources\\base.xls"));
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

            // import data in threads
            Thread usersThread = new Thread(() -> importUsers(workbook));
            Thread vinylsThread = new Thread(() -> importVinyls(workbook));
            Thread booksThread = new Thread(() -> importBooks(workbook));

            vinylsThread.start();
            usersThread.start();
            usersThread.join();
            booksThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importUsers(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.getSheet("users");
        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = rowIterator.next(); // again, first row have headers
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            String name = currentRow.getCell(0).getStringCellValue();
            String surname = currentRow.getCell(1).getStringCellValue();
            String email = currentRow.getCell(2).getStringCellValue();
            int phone = (int) currentRow.getCell(3).getNumericCellValue();
            currentRow.getCell(4).setCellType(CellType.STRING);
            String encodedPassword = currentRow.getCell(4).getStringCellValue();
            byte[] bytesPassword = Base64.getDecoder().decode(encodedPassword);
            String password = new String(bytesPassword);
            String type = currentRow.getCell(5).getStringCellValue();
            if (type.equals("reader")) {
                createReader(name, surname, email, phone, password);
            }
            if (type.equals("admin")) {
                createAdministrator(name, surname, email, phone, password);
            }
        }
    }

    private void importBooks(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.getSheet("books");
        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = rowIterator.next(); // because in first row are headers
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            String title = currentRow.getCell(0).getStringCellValue();
            String author = currentRow.getCell(1).getStringCellValue();
            int pages = (int) currentRow.getCell(2).getNumericCellValue();
            String shortcut = currentRow.getCell(3).getStringCellValue();
            BookGenre genre = BookGenre.getFromShortcut(shortcut);
            Book book = new Book(title, author, pages, genre);
            books.add(book);
            if (currentRow.getCell(4) != null) {
                Date date = currentRow.getCell(4).getDateCellValue();
                LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
                Reader reader = (Reader) getUserFromEmail(currentRow.getCell(5).getStringCellValue());
                reader.borrowBook(book.getId());
                book.setDateOfBorrow(localDateTime);
            }
        }
    }

    private void importVinyls(HSSFWorkbook workbook) {
        HSSFSheet sheet = workbook.getSheet("vinyls");
        Iterator<Row> rowIterator = sheet.rowIterator();
        Row currentRow = rowIterator.next(); // because in first row are headers
        while (rowIterator.hasNext()) {
            currentRow = rowIterator.next();
            String title = currentRow.getCell(0).getStringCellValue();
            String author = currentRow.getCell(1).getStringCellValue();
            String shortcut = currentRow.getCell(2).getStringCellValue();
            MusicGenre genre = MusicGenre.getFromShortcut(shortcut);
            Vinyl vinyl = new Vinyl(title, author, genre);
            vinyls.add(vinyl);
        }
    }

    public void export() {
        try {
            // create workbook
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet vinylsSheet = workbook.createSheet("vinyls");

            // export data in threads
            Thread vinylsThread = new Thread(() -> exportVinyls(vinylsSheet));
            HSSFSheet booksSheet = workbook.createSheet("books");
            Thread booksThread = new Thread(() -> exportBooks(booksSheet));
            HSSFSheet usersSheet = workbook.createSheet("users");
            Thread usersThread = new Thread(() -> exportUsers(usersSheet));

            booksThread.start();
            vinylsThread.start();
            usersThread.start();

            booksThread.join();
            vinylsThread.join();
            usersThread.join();

            // writing everything to workbook
            FileOutputStream outputStream = new FileOutputStream("src\\main\\resources\\base.xls");
            workbook.write(outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportUsers(HSSFSheet sheet) {
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);
        cell.setCellValue("Name");
        cell = row.createCell(1);
        cell.setCellValue("Surname");
        cell = row.createCell(2);
        cell.setCellValue("email");
        cell = row.createCell(3);
        cell.setCellValue("phone number");
        cell = row.createCell(4);
        cell.setCellValue("password");
        cell = row.createCell(5);
        cell.setCellValue("type");

        for (int i = 0; i < users.size(); i++) {
            row = sheet.createRow(i + 1);
            cell = row.createCell(0);
            cell.setCellValue(users.get(i).getName());
            cell = row.createCell(1);
            cell.setCellValue(users.get(i).getSurname());
            cell = row.createCell(2);
            cell.setCellValue(users.get(i).getEmail());
            cell = row.createCell(3);
            cell.setCellValue(users.get(i).getPhoneNumber());
            cell = row.createCell(4);
            String password = users.get(i).getPassword();
            String encodedPassword = Base64.getEncoder().encodeToString(password.getBytes());
            cell.setCellValue(encodedPassword);
            cell = row.createCell(5);
            if (users.get(i) instanceof Reader) {
                cell.setCellValue("reader");
            }
            if (users.get(i) instanceof Administrator) {
                cell.setCellValue("admin");
            }
        }
    }

    private void exportVinyls(HSSFSheet sheet) {
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);
        cell.setCellValue("Title");
        cell = row.createCell(1);
        cell.setCellValue("Author");
        cell = row.createCell(2);
        cell.setCellValue("Genre");
        for (int i=0; i < vinyls.size(); i++) {
            row = sheet.createRow(i + 1);
            cell = row.createCell(0);
            cell.setCellValue(vinyls.get(i).getTitle());
            cell = row.createCell(1);
            cell.setCellValue(vinyls.get(i).getAuthor());
            cell = row.createCell(2);
            cell.setCellValue(vinyls.get(i).getGenre().getShortcut());
        }
    }

    private void exportBooks(HSSFSheet sheet) {
        HSSFFont font = sheet.getWorkbook().createFont();
        font.setCharSet(Font.ANSI_CHARSET);
        HSSFCellStyle style = sheet.getWorkbook().createCellStyle();
        style.setFont(font);
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);
        cell.setCellStyle(style);
        cell.setCellValue("Title");
        cell = row.createCell(1);
        cell.setCellValue("Author");
        cell = row.createCell(2);
        cell.setCellValue("Pages");
        cell = row.createCell(3);
        cell.setCellValue("Genre");
        cell = row.createCell(4);
        cell.setCellValue("DateOfBorrow");
        cell = row.createCell(5);
        cell.setCellValue("Reader");
        for (int i=0; i < books.size(); i++) {
            row = sheet.createRow(i+1);
            cell = row.createCell(0);
            cell.setCellValue(books.get(i).getTitle());
            cell = row.createCell(1);
            cell.setCellValue(books.get(i).getAuthor());
            cell = row.createCell(2);
            cell.setCellValue(books.get(i).getPages());
            cell = row.createCell(3);
            cell.setCellValue(books.get(i).getGenre().getShortcut());
            if (!books.get(i).isAvailable()) {
                cell = row.createCell(4);
                CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
                style.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));
                cell.setCellStyle(style);
                LocalDateTime date = books.get(i).getDateOfBorrow();
                cell.setCellValue(Date.from(date.atZone(ZoneId.systemDefault()).toInstant()));
                cell = row.createCell(5);
                cell.setCellValue(books.get(i).getReader().getEmail());
            }
        }
    }

}
