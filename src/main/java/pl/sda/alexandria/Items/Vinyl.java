package pl.sda.alexandria.Items;

public class Vinyl extends Item{
    private MusicGenre genre;

    public Vinyl(String title, String author, MusicGenre genre) {
        super(title, author);
        this.genre = genre;
        this.type = "vinyl";
    }

    public MusicGenre getGenre() {
        return genre;
    }

}
