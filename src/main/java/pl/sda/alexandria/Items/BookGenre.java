package pl.sda.alexandria.Items;

public enum BookGenre {
    SCIENCE_FICTION("C"),
    FANTASY("F"),
    ADVENTURE ("A"),
    MYSTERY("M"),
    DRAMA("D"),
    ROMANCE("R"),
    HORROR("H"),
    KIDS("K"),
    THRILLER("T"),
    SELF_HELP("S"),
    OTHER("O");

    String shortcut;

    BookGenre(String shortcut) {
        this.shortcut = shortcut;
    }

    public String getShortcut() {
        return shortcut;
    }

    public static  BookGenre getFromShortcut(String shortcut) {
        for (BookGenre genre : BookGenre.values()) {
            if (shortcut.compareTo(genre.getShortcut()) == 0) {
                return genre;
            }
        }
        return OTHER;
    }

}
