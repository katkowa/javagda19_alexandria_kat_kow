package pl.sda.alexandria.Items;

public abstract class Item {
    private Integer id;
    private String title;
    private String description;
    private String author;
    protected boolean available = true;
    protected String type;
    private static int idCounter = 1;

    public Item(String title, String author) {
        this.title = title;
        this.author = author;
        this.id = idCounter;
        idCounter += 1;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthor() {
        return author;
    }

    public boolean isAvailable() {
        return available;
    }

    public boolean isInTitle(String title) {
        boolean result = false;
        if (this.getTitle().toLowerCase().contains(title.toLowerCase())) {
            result = true;
        }
        return result;
    }

    public String getShortDescription() {
        String result = this.getDescription().substring(0,50);
        int lastIndex = result.lastIndexOf(" ");
        return "'" + this.getTitle() + "': " + result.substring(0, lastIndex) + "...";
    }

    @Override
    public String toString() {
        return getId() + ". " + getAuthor() + ", '" + getTitle() + "'";
    }
}
