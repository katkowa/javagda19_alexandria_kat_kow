package pl.sda.alexandria.Items;

import pl.sda.alexandria.users.Reader;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public abstract class ItemToBorrow extends Item implements Borrowable {
    private LocalDateTime dateOfBorrow;
    private LocalDateTime dateOfReturn;
    private Reader reader;

    public ItemToBorrow(String title, String author) {
        super(title, author);
    }

    public Reader getReader() {
        return reader;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    public LocalDateTime getDateOfBorrow() {
        return dateOfBorrow;
    }

    public LocalDateTime getDateOfReturn() {
        return dateOfReturn;
    }

    public void borrow() {
        this.available = false;
        this.dateOfBorrow = LocalDateTime.now();
    }

    @Override
    public void giveBack() {
        this.available = true;
        this.dateOfReturn = LocalDateTime.now();
    }

    public void setDateOfBorrow(LocalDateTime dateOfBorrow) {
        this.dateOfBorrow = dateOfBorrow;
    }

    public void setDateOfReturn(LocalDateTime dateOfReturn) {
        this.dateOfReturn = dateOfReturn;
    }

    public boolean isPeriodExceeded() {
        if (this.getDateOfBorrow() == null) {
            return false;
        }

        int periodInDays = (int) ChronoUnit.DAYS.between(this.getDateOfBorrow().toLocalDate(), LocalDate.now());
        if (periodInDays > Borrowable.MAX_PERIOD) {
            return true;
        }
        return false;
    }
}
