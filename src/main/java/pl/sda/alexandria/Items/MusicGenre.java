package pl.sda.alexandria.Items;

public enum MusicGenre {
    BLUES("B"),
    CHILDREN("CH"),
    CLASSICAL("CL"),
    COUNTRY("CO"),
    DANCE("D"),
    HIPHOP("H"),
    INDIE("I"),
    JAZZ("J"),
    LATIN("L"),
    METAL("M"),
    POP("P"),
    RandB("RB"),
    ROCK("R"),
    REGGAE("RE"),
    OTHER("OT");

    String shortcut;

    MusicGenre(String shortcut) {
        this.shortcut = shortcut;
    }

    public String getShortcut() {
        return shortcut;
    }

    public static  MusicGenre getFromShortcut(String shortcut) {
        for (MusicGenre genre : MusicGenre.values()) {
            if (shortcut.compareTo(genre.getShortcut()) == 0) {
                return genre;
            }
        }
        return OTHER;
    }
}
