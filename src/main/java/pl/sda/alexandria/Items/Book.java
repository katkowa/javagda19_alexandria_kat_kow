package pl.sda.alexandria.Items;

public class Book extends ItemToBorrow{
    private int pages;
    private BookGenre genre;

    public Book(String title, String author, int pages, BookGenre genre) {
        super(title, author);
        this.pages = pages;
        this.genre = genre;
        this.type = "book";
    }

    public int getPages() {
        return pages;
    }

    public BookGenre getGenre() {
        return genre;
    }


}
