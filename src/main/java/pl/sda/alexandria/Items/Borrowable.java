package pl.sda.alexandria.Items;

import java.time.Duration;
import java.time.Period;

public interface Borrowable {
    int MAX_PERIOD = 14; //in days

    void borrow();
    void giveBack();
}
