package pl.sda.alexandria;

import pl.sda.alexandria.Items.Book;
import pl.sda.alexandria.Items.BookGenre;
import pl.sda.alexandria.Items.MusicGenre;
import pl.sda.alexandria.Items.Vinyl;
import pl.sda.alexandria.users.Administrator;
import pl.sda.alexandria.users.Reader;

import java.time.LocalDateTime;
import java.util.List;


public class AlexandriaLibraryTest {

    public static void main(String[] args) {
        AlexandriaLibrary library = new AlexandriaLibrary();

        Administrator admin = library.createAdministrator("Katarzyna", "Kowalska", "kkowalska@pl.sda.pl.sda.alexandria.pl", 123456789, "password");
        Reader reader = library.createReader("Jan", "Nowak", "jnowak@gmail.com", 987654321, "password");

        Book book1 = admin.createBook("A Game of Thrones","George R. R. Martin", 864, BookGenre.FANTASY);
        book1.setDescription("Long ago, in a time forgotten, a preternatural event threw the seasons out of balance. In a land where summers can last decades and winters a lifetime, trouble is brewing. The cold is returning, and in the frozen wastes to the north of Winterfell, sinister forces are massing beyond the kingdom's protective Wall. To the south, the king's powers are failing--his most trusted adviser dead under mysterious circumstances and his enemies emerging from the shadows of the throne. At the center of the conflict lie the Starks of Winterfell, a family as harsh and unyielding as the frozen land they were born to. Now Lord Eddard Stark is reluctantly summoned to serve as the king's new Hand, an appointment that threatens to sunder not only his family but the kingdom itself. ");
        library.addBook(book1);

        Book book2 = admin.createBook("A Clash of Kings", "George R. R. Martin", 761, BookGenre.FANTASY);
        book2.setDescription("A comet the color of blood and flame cuts across the sky. Two great leaders--Lord Eddard Stark and Robert Baratheon--who hold sway over an age of enforced peace are dead, victims of royal treachery. Now, from the ancient citadel of Dragonstone to the forbidding shores of Winterfell, chaos reigns. Six factions struggle for control of a divided land and the Iron Throne of the Seven Kingdoms, preparing to stake their claims through tempest, turmoil, and war. It is a tale in which brother plots against brother and the dead rise to walk in the night. Here a princess masquerades as an orphan boy; a knight of the mind prepares a poison for a treacherous sorceress; and wild men descend from the Mountains of the Moon to ravage the countryside. Against a backdrop of incest and fratricide, alchemy and murder, victory may go to the men and women possessed of the coldest steel...and the coldest hearts. For when kings clash, the whole land trembles. ");
        library.addBook(book2);

        Book book3 = admin.createBook("Clean Code", "Robert C. Martin", 464, BookGenre.SELF_HELP);
        book3.setDescription("Even bad code can function. But if code isn t clean, it can bring a development organization to its knees. Every year, countless hours and significant resources are lost because of poorly written code. But it doesn t have to be that way. \n" +
                "Noted software expert Robert C. Martin presents a revolutionary paradigm with Clean Code: A Handbook of Agile Software Craftsmanship . Martin has teamed up with his colleagues from Object Mentor to distill their best agile practice of cleaning code on the fly into a book that will instill within you the values of a software craftsman and make you a better programmer but only if you work at it. \n" +
                "What kind of work will you be doing? You ll be reading code lots of code. And you will be challenged to think about what s right about that code, and what s wrong with it. More importantly, you will be challenged to reassess your professional values and your commitment to your craft. ");
        library.addBook(book3);

        library.countBooks();
        library.showBooks();

//        reader.borrowBook(1);
//        library.countBooks();
//        library.showAvailableBooks();
//        reader.borrowBook(1);
//        reader.borrowBook(5);
//        reader.borrowBook(3);

        LocalDateTime timeOfBorrow = LocalDateTime.of(2018,10,10,15,0);
        book3.setDateOfBorrow(timeOfBorrow);
        library.showItemsWithExceededPeriod();


//        library.deleteBook(book2);
//        library.countBooks();

        System.out.println("-------------------");
        String title = "queen";
        System.out.println(book1.getTitle() + " contains '" + title + "': " + book1.isInTitle(title));

        System.out.println("Short descriptions:");
        System.out.println(book1.getShortDescription());
        System.out.println(book2.getShortDescription());
        System.out.println(book3.getShortDescription());

        System.out.println("-------------------");

        library.addVinyl(new Vinyl("Beatles For Sale", "The Beatles", MusicGenre.ROCK));
        library.addVinyl(new Vinyl("Stumblin' In", "Chris Norman & Suzi Quatro", MusicGenre.POP));
        library.addVinyl(new Vinyl("African Herbsman", "Bob Marley", MusicGenre.REGGAE));
        library.countVinyls();
        library.showAvailableVinyls();

        System.out.println("-------------------");
        reader.showBorrowedItems();


        System.out.println("-------------------");
        reader.returnItem(book1);
        reader.showBorrowedItems();

        System.out.println("-------------------");
        library.importData();

        System.out.println("-------------------");
        //library.exportBooks();


        System.out.println(BookGenre.FANTASY.getShortcut());


        List<Book> availablebooks = library.getAvailableBooks();
        availablebooks.forEach(book -> System.out.println(book));


    }
}
