package pl.sda.alexandria.javafx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.sda.alexandria.AlexandriaLibrary;
import pl.sda.alexandria.users.Administrator;
import pl.sda.alexandria.users.Reader;
import pl.sda.alexandria.users.User;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.NoSuchElementException;

public class AlexandriaApp extends Application {
    private static AlexandriaLibrary library = new AlexandriaLibrary();
    private static User user;
    private static String controller = null;


    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            library.importData();
        } catch (NoSuchElementException e) {
            AlertBox.display("Database error", "Problem with data import");
        }
        /*
        to login e.g.
        reader email: nowak@mail.com, password: 1234
        admin email: kasia@mail.com, password: 0000
         */
        login();
        try {
            URL resource = getClass().getResource(controller);
            Parent root = FXMLLoader.load(resource);
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setTitle("Alexandria Library");
            primaryStage.show();
            primaryStage.setOnCloseRequest(e -> {
                library.export();
            });
        } catch (Exception e) {
            primaryStage.close();
        }
    }

    public static User getUser() {
        return user;
    }

    public static AlexandriaLibrary getLibrary() {
        return library;
    }

    public void login() {
        user = Login.display();
        if (user == null) {
            Platform.exit();
        }
        if (user instanceof Administrator) {
            controller = "/Admin.fxml";
        } else if (user instanceof Reader) {
            controller = "/Reader.fxml";
        } else {
            Platform.exit();
        }
    }

}
