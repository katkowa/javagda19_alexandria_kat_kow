package pl.sda.alexandria.javafx;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.sda.alexandria.users.Reader;

public class BorrowBook extends AlexandriaApp{
    public static void display(Reader reader) {
        Stage window = new Stage();
        window.setAlwaysOnTop(true);

        window.setTitle("Borrow book");
        window.setMinWidth(550);

        Label label = new Label();
        label.setText("Enter ID of the book you want to borrow:");
        TextField textField = new TextField();
        textField.setPromptText("e.g. 1");

        Button borrowButton = new Button("Borrow");
        Button closeButton = new Button("Close");

        borrowButton.setOnAction(e -> {
            try {
                int Id = Integer.parseInt(textField.getText());
                reader.borrowBook(Id);
                window.close();
            } catch (NullPointerException npe) {
                AlertBox.display("Ups", npe.getMessage());
            }catch (RuntimeException re) {
                AlertBox.display("Upss", "Invalid number");
            }
        });

        closeButton.setOnAction(e -> window.close());

        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(20,20,20,20));
        vBox.getChildren().addAll(label, textField, borrowButton, closeButton);

        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.show();
    }
}
