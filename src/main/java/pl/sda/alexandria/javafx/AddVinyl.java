package pl.sda.alexandria.javafx;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.sda.alexandria.AlexandriaLibrary;
import pl.sda.alexandria.Items.MusicGenre;
import pl.sda.alexandria.Items.Vinyl;
import pl.sda.alexandria.users.Administrator;

public class AddVinyl {
    static void display(AlexandriaLibrary library, Administrator admin) {
        Stage window = new Stage();
        window.setTitle("Add Vinyl");
        window.setMinWidth(550);
        window.setAlwaysOnTop(true);

        Label titleLabel = new Label("Title:");
        TextField titleText = new TextField();
        Label authorLabel = new Label("Author:");
        TextField authorText = new TextField();
        Label genreLabel = new Label("Genre:");
        ChoiceBox<MusicGenre> genreBox = new ChoiceBox<>();
        genreBox.getItems().addAll(MusicGenre.values());
        Button addButton = new Button("Add vinyl");

        addButton.setOnAction(e -> {
            try  {
                String title = titleText.getText();
                String author = authorText.getText();
                MusicGenre genre = genreBox.getValue();
                library.addVinyl(admin.createVinyl(title, author, genre));
                window.close();
            } catch (RuntimeException re) {
                AlertBox.display("Invalid data", "Something is wrong with your inputs :o Try again");
            }
        });

        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(20, 20, 20, 20));
        vBox.getChildren().addAll(titleLabel, titleText, authorLabel, authorText, genreLabel, genreBox, addButton);

        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.show();
    }
}
