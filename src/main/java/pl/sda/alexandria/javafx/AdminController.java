package pl.sda.alexandria.javafx;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import pl.sda.alexandria.AlexandriaLibrary;
import pl.sda.alexandria.Items.Item;
import pl.sda.alexandria.users.Administrator;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminController implements Initializable {
    @FXML
    private Button createReaderButton;
    @FXML
    private Button createAdminButton;
    @FXML
    private Button addBookButton;
    @FXML
    private Button deleteBookButton;
    @FXML
    private Button addVinylButton;
    @FXML
    private Button deleteVinylButton;
    @FXML
    private Button displayBooksButton;
    @FXML
    private Button displayAvailableBooksButton;
    @FXML
    private Button displayVinylsButton;
    @FXML
    private Button displayExceededItemsButton;
    @FXML
    private TableView<Item> table = new TableView<>();
    @FXML
    private TableColumn<Item, Integer> idColumn;
    @FXML
    private TableColumn<Item, String> titleColumn;
    @FXML
    private TableColumn<Item, String> authorColumn;
    @FXML
    private TableColumn<Item, String> availableColumn;
    @FXML
    Label tableLabel;
    AlexandriaLibrary library;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        library = AlexandriaApp.getLibrary();
        Administrator admin = (Administrator) AlexandriaApp.getUser();
        ItemTable itemTable = new ItemTable(table, tableLabel, idColumn, titleColumn, authorColumn, availableColumn);
        tableLabel.setText("Items with exceeded period:");
        itemTable.display(library.getItemsWithExceededPeriod(), "Items with exceeded period:");

        createReaderButton.setOnAction(e -> CreateUser.display(library, "reader"));
        createAdminButton.setOnAction(e -> CreateUser.display(library, "admin"));
        addBookButton.setOnAction(e -> AddBook.display(library, admin));
        deleteBookButton.setOnAction(e -> DeleteItem.display(library, "book"));
        addVinylButton.setOnAction(e -> AddVinyl.display(library, admin));
        deleteVinylButton.setOnAction(e -> DeleteItem.display(library, "vinyl"));

        displayBooksButton.setOnAction(e -> itemTable.display(library.getBooks(), "Books:"));
        displayAvailableBooksButton.setOnAction(e -> itemTable.display(library.getAvailableBooks(), "Available Books:"));
        displayVinylsButton.setOnAction(e -> itemTable.display(library.getVinyls(), "Vinyls:"));
        displayExceededItemsButton.setOnAction(e -> itemTable.display(library.getItemsWithExceededPeriod(), "Items with Exceeded Period:"));

    }
}
