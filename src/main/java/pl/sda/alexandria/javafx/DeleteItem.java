package pl.sda.alexandria.javafx;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.sda.alexandria.AlexandriaLibrary;
import pl.sda.alexandria.Items.Book;
import pl.sda.alexandria.Items.Vinyl;

public class DeleteItem {
    static void display(AlexandriaLibrary library, String type) {
        Stage window = new Stage();
        window.setTitle("Delete " + type);
        window.setMinWidth(550);
        window.setAlwaysOnTop(true);

        Label label = new Label("Enter ID number of the " + type + " you want to delete:");
        TextField textField = new TextField();
        Button button = new Button("Delete " + type);

        button.setOnAction(e -> {
            try {
                int id = Integer.parseInt(textField.getText());
                library.deleteItem(id);
                window.close();
            } catch (NumberFormatException nfe) {
                AlertBox.display("Invalid number", "Invalid format of number. Try again!");
            } catch (Exception ex) {
                AlertBox.display("Invalid number", ex.getMessage());
            }
        });

        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(20, 20, 20, 20));
        vBox.getChildren().addAll(label, textField, button);

        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.show();
    }

}
