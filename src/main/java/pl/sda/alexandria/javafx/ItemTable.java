package pl.sda.alexandria.javafx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.sda.alexandria.Items.Item;

import java.util.List;

public class ItemTable {
    @FXML
    private TableView<Item> table;
    private Label label;
    private TableColumn<Item, Integer> idColumn;
    private TableColumn<Item, String> titleColumn;
    private TableColumn<Item, String> authorColumn;
    private TableColumn<Item, String> availableColumn;

    public ItemTable(TableView<Item> table, Label label, TableColumn<Item, Integer> idColumn, TableColumn<Item, String> titleColumn,
                     TableColumn<Item, String> authorColumn, TableColumn<Item, String> availableColumn ) {
        this.table = table;
        this.label = label;
        this.idColumn = idColumn;
        this.authorColumn = authorColumn;
        this.titleColumn = titleColumn;
        this.availableColumn = availableColumn;

    }

    public <T extends Item> void display(List<T> items, String title) {
        label.setText(title);
        ObservableList<Item> list = get(items);
        if (list.size() != 0) {
            idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
            titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
            authorColumn.setCellValueFactory(new PropertyValueFactory<>("author"));
            availableColumn.setCellValueFactory(new PropertyValueFactory<>("available"));
            table.setItems(list);
        }
    }

    public <T extends Item> ObservableList<Item> get(List<T> list){
        ObservableList<Item> result = FXCollections.observableArrayList();
        result.addAll(list);
        return result;
    }


}
