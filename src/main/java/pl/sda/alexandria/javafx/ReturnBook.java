package pl.sda.alexandria.javafx;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.sda.alexandria.AlexandriaLibrary;
import pl.sda.alexandria.Items.Book;
import pl.sda.alexandria.users.Reader;

public class ReturnBook {
        public static void display(Reader reader, AlexandriaLibrary library) {
            Stage window = new Stage();
            window.setTitle("Return book");
            window.setMinWidth(550);
            window.setAlwaysOnTop(true);

            Label label = new Label();
            label.setText("Enter ID of the book you want to return");
            TextField textField = new TextField();
            textField.setPromptText("e.g. 1");

            Button borrowButton = new Button("Return");
            Button closeButton = new Button("Close");

            borrowButton.setOnAction(e -> {
                try {
                    int Id = Integer.parseInt(textField.getText());
                    Book book = (Book) library.getFromId(Id);
                    if (reader.getBorrowedItems().contains(book)) {
                        reader.returnItem(book);
                        window.close();
                    } else {
                        AlertBox.display("Invalid id number", "You don't have this item.");
                    }
                } catch (RuntimeException re) {
                    AlertBox.display("Upss", "Invalid number");
                }
            });

            closeButton.setOnAction(e -> window.close());

            VBox vBox = new VBox(10);
            vBox.setPadding(new Insets(20,20,20,20));
            vBox.getChildren().addAll(label, textField, borrowButton, closeButton);

            Scene scene = new Scene(vBox);
            window.setScene(scene);
            window.show();
        }
    }

