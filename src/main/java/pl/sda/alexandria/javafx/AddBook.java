package pl.sda.alexandria.javafx;

import com.sun.media.sound.InvalidFormatException;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.sda.alexandria.AlexandriaLibrary;
import pl.sda.alexandria.Items.Book;
import pl.sda.alexandria.Items.BookGenre;
import pl.sda.alexandria.users.Administrator;

public class AddBook {
    static void display(AlexandriaLibrary library, Administrator admin) {
        Stage window = new Stage();
        window.setTitle("Add book");
        window.setMinWidth(550);
        window.setAlwaysOnTop(true);

        Label titleLabel = new Label("Title:");
        TextField titleText = new TextField();
        Label authorLabel = new Label("Author:");
        TextField authorText = new TextField();
        Label pagesLabel = new Label("Number of pages");
        TextField pagesText = new TextField();
        Label genreLabel = new Label("Genre:");
        ChoiceBox<BookGenre> genreBox = new ChoiceBox<>();
        genreBox.getItems().addAll(BookGenre.values());

        Button addButton = new Button("Add Book");
        addButton.setOnAction(e -> {
            try {
                String title = titleText.getText();
                String author = authorText.getText();
                int pages = Integer.parseInt(pagesText.getText());
                BookGenre genre = BookGenre.OTHER;
                genre = genreBox.getValue();
                library.addBook(admin.createBook(title, author, pages, genre));
                window.close();
                } catch (NumberFormatException nfe) {
                AlertBox.display("Invalid data", "'Number of pages' fiels must be a number");
            }
        });

        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(20, 20, 20, 20));
        vBox.getChildren().addAll(titleLabel, titleText, authorLabel, authorText, pagesLabel, pagesText, genreLabel, genreBox, addButton);

        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.show();
    }
}
