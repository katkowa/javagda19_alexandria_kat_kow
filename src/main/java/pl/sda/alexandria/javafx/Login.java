package pl.sda.alexandria.javafx;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.sda.alexandria.AlexandriaLibrary;
import pl.sda.alexandria.users.Reader;
import pl.sda.alexandria.users.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login {
    static AlexandriaLibrary library = new AlexandriaLibrary();
    static User result = null;
    private static Stage window = new Stage();

    static User display() {
        window.setAlwaysOnTop(true);
        window.setTitle("Login");
        window.setMinWidth(500);

        Label label = new Label("Welcome to Alexandria Library!");

        Label emailLabel = new Label("E-mail address:");
        TextField emailText = new TextField();

        Label passwordLabel = new Label("Password:");
        PasswordField passwordText = new PasswordField();

        Button button = new Button("Sign in");
        button.setOnAction(e -> {
            // check if e-mail format is correct
            Pattern emailPattern = Pattern.compile("[a-zA-Z_0-9]+@[a-zA-z]+\\.[a-z]+");
            Matcher matcher = emailPattern.matcher(emailText.getText());
            if (matcher.matches()) {
                try {
                    result = findUser(library, emailText.getText(), passwordText.getText());
                    window.close();
                } catch (RuntimeException re) {
                    LoginAlertBox.display("Invalid data", re.getMessage());
                }
            } else {
                LoginAlertBox.display("Invalid data", "Invalid e-mail format");
            }
        });

        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(20,20,20,20));
        vBox.getChildren().addAll(label,emailLabel, emailText, passwordLabel, passwordText, button);
        vBox.setAlignment(Pos.CENTER);

        //Display window and wait for it to be closed before returning
        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.showAndWait();

        window.setOnCloseRequest(e -> result = null);
        return result;
    }

    public static User findUser(AlexandriaLibrary library, String email, String password) {
        for (User user : library.getUsers()) {
            if (user.getEmail().equals(email)) {
                if (user.getPassword().equals(password)) {
                    return user;
                } else {
                    throw new RuntimeException("Invalid password");
                }
            }
        }
        throw new RuntimeException("Invalid e-mail");
    }

    public static Stage getWindow() {
        return window;
    }
}

