package pl.sda.alexandria.javafx;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import pl.sda.alexandria.AlexandriaLibrary;
import pl.sda.alexandria.Items.Item;
import pl.sda.alexandria.users.Reader;

import java.net.URL;
import java.util.ResourceBundle;

public class ReaderController implements Initializable {
    @FXML
    private Button displayBooksButton;
    @FXML
    private Button displayAvailableBooksButton;
    @FXML
    private Button displayVinylsButton;
    @FXML
    private Button borrowBookButton;
    @FXML
    private Button returnBookButton;
    @FXML
    private Button displayBorrowedItemsButton;
    @FXML
    private TableColumn<Item, Integer> idColumn;
    @FXML
    private TableColumn<Item, String> titleColumn;
    @FXML
    private TableColumn<Item, String> authorColumn;
    @FXML
    private TableColumn<Item, String> availableColumn;
    @FXML
    private TableView<Item> table = new TableView<>();
    @FXML
    private Label tableLabel = new Label();

    AlexandriaLibrary library;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        library = AlexandriaApp.getLibrary();
        ItemTable itemTable = new ItemTable(table, tableLabel, idColumn, titleColumn, authorColumn, availableColumn);
        Reader reader = (Reader) AlexandriaApp.getUser();
        tableLabel.setText("Your items:");
        itemTable.display(reader.getBorrowedItems(), "Your items:");
        borrowBookButton.setOnAction(e -> BorrowBook.display(reader));
        returnBookButton.setOnAction(e -> ReturnBook.display(reader, library));
        displayBorrowedItemsButton.setOnAction(e -> itemTable.display(reader.getBorrowedItems(), "Your items"));
        displayBooksButton.setOnAction(e -> itemTable.display(library.getBooks(), "Books:"));
        displayAvailableBooksButton.setOnAction(e -> itemTable.display(library.getAvailableBooks(), "Available books:"));
        displayVinylsButton.setOnAction(e -> itemTable.display(library.getVinyls(), "Vinyls:"));
    }
}
