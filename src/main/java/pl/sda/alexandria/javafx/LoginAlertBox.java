package pl.sda.alexandria.javafx;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class LoginAlertBox {
    static void display(String title, String message) {
        Stage stage = new Stage();

        //Block events to other windows
        //stage.initModality(Modality.APPLICATION_MODAL);
        stage.setAlwaysOnTop(true);
        stage.setTitle(title);
        stage.setMinWidth(350);

        Label label = new Label();

        label.setText(message);

        Button closeButton = new Button("Sorry, I will try again");
        closeButton.setOnAction(e -> {
            Login.getWindow().close();
            Login.display();
            stage.close();
        });

        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(20,20,20,20));
        vBox.getChildren().addAll(label, closeButton);
        vBox.setAlignment(Pos.CENTER);

        //Display stage and wait for it to be closed before returning
        Scene scene = new Scene(vBox);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
