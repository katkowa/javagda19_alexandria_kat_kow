package pl.sda.alexandria.javafx;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.sda.alexandria.AlexandriaLibrary;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CreateUser {
    static void display(AlexandriaLibrary library, String typeOfUser) {
        Stage window = new Stage();
        window.setTitle("Create " + typeOfUser);
        window.setMinWidth(550);
        window.setAlwaysOnTop(true);

        Label nameLabel = new Label("Name:");
        TextField nameText = new TextField();
        nameText.setPromptText("e.g. Jan");

        Label surnameLabel = new Label("Surname");
        TextField surnameText = new TextField();
        surnameText.setPromptText("e.g. Nowak");

        Label emailLabel = new Label("E-mail adress:");
        TextField emailText = new TextField();
        emailText.setPromptText("e.g. jannowak@gmail.com");

        Label phoneLabel = new Label("Phone number:");
        TextField phoneText = new TextField();
        phoneText.setPromptText("e.g. 123456789");

        Label passwordLabel = new Label("Password:");
        PasswordField passwordText = new PasswordField();

        Button addButton = new Button("Add user");
        addButton.setOnAction(e -> {
            try {
                String name = nameText.getText();
                Pattern pattern = Pattern.compile("[a-zA-z]+");
                Matcher matcher = pattern.matcher(name);
                if (!matcher.matches()) {
                    throw new IllegalArgumentException("Invalid name format");
                }
                String surname = surnameText.getText();
                matcher = pattern.matcher(surname);
                if (!matcher.matches()) {
                    throw new IllegalArgumentException("Invalid surname format");
                }
                String email = emailText.getText();
                pattern = Pattern.compile("[a-zA-Z_0-9]+@[a-zA-z]+\\.[a-z]+");
                matcher = pattern.matcher(email);
                if (!matcher.matches()) {
                    throw new IllegalArgumentException("Invalid e-mail format");
                }
                int phone = Integer.parseInt(phoneText.getText());
                pattern = Pattern.compile("[0-9]{9}");
                matcher = pattern.matcher(String.valueOf(phone));
                if (!matcher.matches()) {
                    throw new IllegalArgumentException("Invalid phone number format.");
                }
                String password = passwordText.getText();
                matcher = Pattern.compile(".{6,}").matcher(password);
                if (!matcher.matches()) {
                    throw new IllegalArgumentException("The password must be at least 6 characters long");
                }
                if (typeOfUser == "admin") {
                    library.createAdministrator(name, surname, email, phone, password);
                } else if (typeOfUser == "reader") {
                    library.createReader(name, surname, email, phone, password);
                }
                window.close();
            } catch (RuntimeException re) {
                AlertBox.display("Invalid data", re.getMessage());
            }
        });

        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(20, 20, 20, 20));
        vBox.getChildren().addAll(nameLabel, nameText, surnameLabel, surnameText, emailLabel, emailText, phoneLabel, phoneText, passwordLabel, passwordText, addButton);

        Scene scene = new Scene (vBox);
        window.setScene(scene);
        window.show();
    }
}
