package pl.sda.alexandria.javafx;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertBox {
    static void display(String title, String message) {
        Stage window = new Stage();

        //Block events to other windows
        //window.initModality(Modality.APPLICATION_MODAL);
        window.setAlwaysOnTop(true);
        window.setTitle(title);
        window.setMinWidth(250);

        Label label = new Label();

        label.setText(message);

        Button closeButton = new Button("Sorry, I will try again");
        closeButton.setOnAction(e -> window.close());

        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(20,20,20,20));
        vBox.getChildren().addAll(label, closeButton);
        vBox.setAlignment(Pos.CENTER);

        //Display window and wait for it to be closed before returning
        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.showAndWait();
    }
}
